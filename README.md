# Linear Algebra Cheatsheet

This is the cheatsheet for linear algebra that was created in cooperation with jpiekarek, vkaas and fpomberger.

No guarantee regarding correctness ;)

## How to build

This project contains a Makefile which makes it trivial to build this project on operating systems which have [GNU](https://gnu.org/) and [Tex Live](https://tug.org/texlive) (or equivalent provider of an `pdflatex` executable) installed.

In such an environment you can build the project using the `make` command
```sh
make build
```
And if you want to open the pdf afterwards you can use the following command
```bash
make show
```

