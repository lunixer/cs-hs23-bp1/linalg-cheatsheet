build: *.tex
	@echo "Building project..."
	@if [ ! -d "./build" ]; then mkdir build; fi
	pdflatex -halt-on-error -file-line-error -output-directory=./build index.tex
	@echo "Done."

clean: 
	@echo "Cleaning build directory..."
	@if [ -d "./build" ]; then rm -r build; fi
	@echo "Done."

show: build
	echo "xdg-open build/index.pdf &"
	@xdg-open build/index.pdf 2> /dev/null 1> /dev/null &
